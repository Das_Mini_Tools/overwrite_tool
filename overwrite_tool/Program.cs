﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace overwrite_tool
{
    internal class Program
    {
        private static void Error(string error)
        {
            AssemblyName assembly = Assembly.GetExecutingAssembly().GetName();

            Console.WriteLine(string.Format("{0} v{1} by Dasanko, written for catalinnc", assembly.Name, assembly.Version));

            Console.WriteLine();

            Console.WriteLine("ERROR: " + error + ". Usage example:");

            Console.WriteLine(assembly.Name + " <100|0x20|append|end|start> targetFile sourceFile");

            Console.WriteLine();
        }

        private static bool IsInputValid(string[] args)
        {
            if (args.Length != 3)
            {
                Error("Invalid number of arguments");

                return false;
            }

            if (!File.Exists(args[1]))
            {
                Error("The target file does not exist");

                return false;
            }

            if (!File.Exists(args[2]))
            {
                Error("The source file does not exist");

                return false;
            }

            if (args[1].ToLower() == args[2].ToLower())
            {
                Error("The source and target files can't be the same");

                return false;
            }

            return true;
        }

        private static void Main(string[] args)
        {
            if (!IsInputValid(args))
                return;

            long offset = ParseOffset(args[0], args[1], args[2]);

            if (offset < 0)
            {
                Warn("The source file is bigger than the target file. The target offset will be set to 0");

                return;
            }

            Write(offset, args[1], args[2]);
        }

        private static long ParseOffset(string offset, string targetFile, string sourceFile)
        {
            if (offset.ToLower() == "start")
                return 0;

            if (offset.ToLower() == "end")
                return new FileInfo(targetFile).Length - new FileInfo(sourceFile).Length;

            if (offset.ToLower() == "append")
                return new FileInfo(targetFile).Length;

            if (offset.ToLower().Contains("x"))
                return long.Parse(offset.Substring(2), NumberStyles.HexNumber);

            return long.Parse(offset);
        }

        private static void Warn(string warning)
        {
            Console.WriteLine("WARNING: " + warning + ".");

            Console.WriteLine();
        }

        private static void Write(long offset, string targetFile, string sourceFile)
        {
            Console.WriteLine("Working... please wait...");

            using (var tfr = File.OpenWrite(targetFile))
            using (var sfr = File.OpenRead(sourceFile))
            {
                int readBytes;
                byte[] buffer = new byte[4096];

                tfr.Position = offset;

                while ((readBytes = sfr.Read(buffer, 0, buffer.Length)) > 0)
                    tfr.Write(buffer, 0, readBytes);
            }

            Console.WriteLine("Work complete!");
        }
    }
}
